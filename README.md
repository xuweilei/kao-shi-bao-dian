# 考試寶典
您還在為考試而焦慮嗎？考試寶典這個app可以讓您更好做好考試的準備，它提供了章節練習，歷年真題，模擬練習，考前押題，錯題回顧，考點匯總等壹系列功能，是您考試的好幫手。

Are you still anxious about the exam? The exam book app can help you better prepare for the exam. It provides chapter exercises, calendar questions, simulation exercises, pre-examination vocabulary, wrong question review, test point summary and other functions. 
It is a good helper for your exam.